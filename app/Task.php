<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model {

	

	protected $fillable = [
		'name',
		'description',
		'project_id',
		'status'
	];

	public function project()
	{
		return $this->belongsTo('App\Project');
	}

	/*
	* @return Response
	*/
	public function task_comments()
	{
		return $this->hasMany('App\TaskComment');
	}

	public function user_task()
	{
		return $this->hasOne('App\UserTask');
	}

}

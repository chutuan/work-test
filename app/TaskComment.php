<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class TaskComment extends Model
{

	protected $fillable = [
		'task_id',
		'comment_id'
	];

	public function task()
	{
		return $this->belonsTo('App\Task');
	}

	public function comment()
	{
		return $this->belongsTo('App\Comment');
	}

}

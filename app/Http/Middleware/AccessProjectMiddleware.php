<?php namespace App\Http\Middleware;

use Closure;

class AccessProjectMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(\Auth::user())
		{
			if(!\Auth::user()->manager)
			{
				if(!\Auth::user()->user_projects()->where('project_id', $request->route()
					->getParameter('projects')->id)->first())
				{
					return redirect('home');
				}

			}
		}
		else {
			return redirect()->guest('auth/login');
		}
		
		return $next($request);
	}

}

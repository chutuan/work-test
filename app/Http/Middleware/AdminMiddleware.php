<?php namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\RedirectResponse;
class AdminMiddleware {

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if(\Auth::user())
		{
			if(!\Auth::user()->manager)
			{
				return "Not perrmission!";
				//return new RedirectResponse(url('/home'));
			}
		}
		else {
			return redirect()->guest('auth/login');
			//return new RedirectResponse(url('/home'));
		}

		return $next($request);
	}

}

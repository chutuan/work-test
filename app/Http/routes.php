<?php
use App\Project;
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'ProjectController@index');

Route::get('home', 'ProjectController@index');

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

//Asign member to Task
Route::post('/task/assign', 'TaskController@assign');
Route::resource('projects.tasks.comments', 'CommentController');

Route::resource('projects.tasks', 'TaskController');
Route::resource('projects', 'ProjectController');

Route::group(['middleware' => 'manager', 'prefix' => 'admin'], function()
{
	Route::resource('projects', 'AdminProjectController');
});

Route::group(['prefix' => 'api'], function()
{
	Route::get('autocomplete', 'APIController@autocompleteUser');
});

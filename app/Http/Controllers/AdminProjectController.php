<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Project;
use App\User;

class AdminProjectController extends Controller {


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		$projects = Project::paginate(10);

		return view('admin.projects.index')->with('projects', $projects);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('admin.projects.create');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($project)
	{
		$users =  array();
		foreach ($project->user_projects()->get() as $userProject)
		{
			array_push($users, $userProject->user()->first()->email);
		}
		return view('admin.projects.show', compact('users', 'project'));
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($project, Request $request)
	{
		$user = User::where('email', $request->get('user'))->first();
		if($user->user_projects()->find($project->id))

		{
			return redirect('/admin/projects/' . $project->id);
		}
		$user->user_projects()
					->create(['project_id' => $project->id]);

		return redirect('/admin/projects/' . $project->id);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Requests\ProjectRequest $request)
	{
			Project::create($request->all());

			return redirect('admin/projects');
	}



}

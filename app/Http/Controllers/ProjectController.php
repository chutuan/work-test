<?php namespace App\Http\Controllers;

use App\Http\Requests as Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Project;

class ProjectController extends Controller {

	public function __construct()
	{
		$this->middleware('auth', ['only' => ['index']]);
		$this->middleware('accessProject', ['only' => ['show']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		if(\Auth::user()->manager)
			$projects = Project::all();
		else
		{
			$projects = array();
			$userProjects = \Auth::user()->user_projects()->get();
			foreach($userProjects as $userProject)
				array_push($projects, $userProject->project()->get()->first());
		}


		return view('projects.index')->with('projects', $projects);
	}



	

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($project)
	{
		$taskFirst = $project->tasks()->first();
		return view('projects.show', compact('taskFirst', 'project'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

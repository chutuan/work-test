<?php namespace App\Http\Controllers;

use App\Http\Requests as Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Task;
use App\User;
use App\UserTask;
class TaskController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$tasks = Task::all();
		return view('tasks.index')->with('tasks', $tasks);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($project)
	{
		return view('tasks.create')->with('project', $project);
	}

	/**
	 * Assign member to task.
	 *
	 * @return Response
	 */
	public function assign(Request $request)
	{
		UserTask::where('task_id', '=', $request->get('task_id'))->delete();

		$user = User::where('email', $request->get('email'))->first();

		$userTask = $user->user_tasks()
					->create(['task_id' => $request->get('task_id')]);
		return $user;
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($project, Requests\TaskRequest $request)
	{

		$project->tasks()->create($request->all());

		return redirect ('projects/' . $project->id);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($project, $taskID)
	{
		$task = $project->tasks()->find($taskID);
		return view('tasks.show')->with(['project' => $project, 'task' => $task]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit( $projects, $taskID)
	{
		$task = Task::findOrFail($taskID);

		return view('tasks.edit')->with(['task' => $task, 'project_id' => $projects]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($projects, $tasks,Request $request)
	{
		$task = Task::findOrFail($tasks);

		$task->update($request->all());

		return $task;
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}

<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;


use App\Http\Requests;
use Request;
use DB;


class APIController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function autocompleteUser()
	{
		$query = Request::input('query');

		$data =  DB::table('users')
							->where('name', 'LIKE', $query . '%')
							->orWhere('email', 'LIKE', $query . '%')
							->take(5)->get();
		$result_array = array();
		foreach ($data as $value)
		{
			$push = array('value' => $value->email, 'name' => $value->name);
			array_push($result_array, $push );
		}
		return array('suggestions' => $result_array);
	}


}

@extends('project')

@section('content')

    <div class="row text-left">
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-3">
            To Do
          </div>
          <div class="col-md-3">
            In Progress
          </div>
          <div class="col-md-3">
            Ready to review
          </div>
          <div class="col-md-3">
            Done
          </div>
        </div>

      </div>

    </div>
    <div class="row" id="list-box-task">
      <div class="col-md-9">
        @for ($i = 0;  $i < 4; $i++)
        <div class="col-md-3 ">
          <div class="column">
            @if($project->tasks)
              @foreach ($project->tasks as $task)
                @if ($task->status == $i)
                <div class="box-task">
                  <div class="row">
                    {!! Form::hidden('task_id', $task->id) !!}
                    <div class="col-md-10">
                      <a href="">{{$task->name}}</a>
                      <span>  {{$task->description}}</span>
                    </div>
                    <div class="col-md-2 n-p">
                    <a>
                    @if($task->user_task()->first())
                      @if($task->user_task()->first()->user()->first()->avatar)
                        <img class="img-responsive"
                        src="../imgs/avatar/{{$task->user_task()->first()->user()->first()->avatar}}"
                        alt="" title="{{$task->user_task()->first()->user()->first()->name}}">
                      @else
                        <img class="img-responsive"
                        src="../imgs/avatar/icon.jpg"
                        alt="" title="{{$task->user_task()->first()->user()->first()->name}}">
                      @endif
                    @else
                      <img class="img-responsive"
                      src="../imgs/avatar/icon.jpg"
                      alt="">
                    @endif
                  </a>
                    </div>
                  </div>
                </div>
                @endif
              @endforeach
            @endif
          </div>
        </div>
        @endfor
      </div>
      <div class="col-md-3" id="box-task-detail">
        <div class="overlay loading hidden">
          <img src="/imgs/loading.gif" alt="">
        </div>
        <div class="task-content">
          @include('tasks.show',  array('project' => $project, 'task' => $taskFirst))
        </div>
      </div>
    </div>
    <div class="row">
      <a href="{{ action('TaskController@create', $project->id) }}" class="btn alert-success">Add Task</a>
    </div>

    @include('modals.assign')

@endsection

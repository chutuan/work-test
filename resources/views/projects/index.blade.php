@extends('app')

@section('content')
  <h2>List your project:</h2>

  <ul class="list-unstyled">
    @foreach ($projects as $project)
    <hr>
    <li> <a href="{{ action('ProjectController@show', $project->id)}}">{{$project->name}}</a> </li>
    @endforeach
  </ul>
  <hr>


@endsection

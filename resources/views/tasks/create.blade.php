@extends('app')

@section('content')
  <h1> Create Task </h1>
  @if ($errors->any())
    <div class="alert alert-danger">
      <ul class="list-unstyled">
        @foreach ($errors->all() as $error)
        <li>{{ $error }}</li>
        @endforeach
      </ul>
    </div>
  @endif
  {!! Form::open(['action' => ['TaskController@index', $project->id]]) !!}
    @include('tasks.partials.form', ['submitButtonText' => 'Create Task'])
  {!! Form::close() !!}
@endsection

@extends('app')

@section('content')

  @foreach($tasks as $task)
    <div class="row">
      <div class="col-md-12">
        <h1> <a href="{{ action('TaskController@show', $task->id) }}">{{ $task->name }}</a> </h1>
        <ul class="list-inline list-unstyled">
          <li><a href="" class="btn alert-success">Edit</a></li>
          <li><a href="#" class="btn alert-danger">Delete</a></li>
        </ul>
      </div>
    </div>
    <hr>
  @endforeach
@endsection

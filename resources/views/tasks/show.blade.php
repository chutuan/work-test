
<div class="task-title">
  @if($task)
    {{ $project->name}} / <a href="">{{ $task->name }}</a>
  @endif
</div>
<hr>
<div class="box-comment">

  <ul class="list-unstyled">
    @if($task)
      @foreach ($task->task_comments()->get() as $taskComment)
        <li><a href="#">{{ $taskComment->comment()->first()->user()->first()->name }} </a>{{ $taskComment->comment()->first()->content }}</li>
      @endforeach
    @endif
  </ul>
  <hr>
  {!! Form::open(['action' => ['CommentController@index', $project->id, ($task) ? $task->id : 0]]) !!}
    <div class="form-group">
      {!! Form::text('content', null, ['class' => 'form-control'] ) !!}
    </div>
    {!! Form::submit('Add Comment', ['class' => 'btn btn-primary']) !!}
  {!! Form::close() !!}

</div>
<script type="text/javascript">
  // AJAX ADD Comment
  $('.box-comment form').submit(function(e)
  {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: $(this).serialize(),
      success: function(data)
      {
        $('.box-comment form').find('input[type="text"]').val('');
        $('.box-comment ul').append('<li><a>' + data.user.name + '</a>'+  data.content +  '</li>')
      }
    });
  });
</script>

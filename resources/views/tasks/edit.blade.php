@extends('app')

@section('content')
  <h1> Create Task </h1>
  @include('errors.list')

  {!! Form::model($task, ['method' => 'PATCH', 'action' => ['TaskController@update', $project_id, $task->id]]) !!}
    @include('tasks.partials.form', ['submitButtonText' => 'Update Task'])
  {!! Form::close() !!}
@endsection

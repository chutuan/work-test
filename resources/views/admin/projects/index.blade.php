@extends('app')

@section('content')
  <h1>List Project</h1>
  <hr>
  <div class="list-project">
    @foreach ($projects as $project)
    <div class="row">
      <div class="col-md-4">
        <h3>
          <a href="{{ action('AdminProjectController@show', $project->id)}}">
            {{ $project->name }}
          </a>
        </h3>
      </div>
    </div>
    @endforeach

  </div>
  <hr>

  <nav>
  <ul class="pagination">
    <li>
      <a href="{{ $projects->url($projects->currentPage() - 1) }}" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
      </a>
    </li>
    @for ($index = 1;  $index <= $projects->lastPage(); $index ++)
    <li class="{{ ($index == $projects->currentPage()) ? 'active' : ''  }}"><a href="{{ $projects->url($index) }}">{{ $index }}</a></li>
    @endfor
    <li>
      <a href="{{ $projects->url($projects->currentPage() + 1) }}" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
      </a>
    </li>
  </ul>
  <hr>
  <a href="{{action('AdminProjectController@create')}}" class="btn alert-success">Create Project</a>
</nav>
@endsection

@extends('app')

@section('content')

    <h1>Create Project</h1>
    @include('errors.list')

    {!! Form::open(['action' => ['AdminProjectController@index']]) !!}
        @include('admin.projects.form', ['submitButton' => 'Create Project'])
    {!! Form::close() !!}
@endsection

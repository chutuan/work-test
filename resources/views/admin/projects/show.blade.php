@extends('app')

@section('content')
  <h1>{{ $project->name }}</h1>
  <p>
    {{ $project->description }}
  </p>
  <hr>
  <h3><strong>Users join project: </strong></h3>
  <ul class="list-unstyled">
    @foreach ($users as $user)
    <li ><img style="max-width: 40px; margin: 10px 0px;" src="/imgs/avatar/icon.jpg" alt="" />{{ $user }}</li>
    @endforeach
  </ul>
  <hr>
  {!! Form::open(['method' => 'PUT']) !!}
  <div class="form-group">
    <label for="user"><strong>Add User To Project</strong></label>
    <input type="text" name="user" id="autocomplete-user" value="" class="form-control">

  </div>
  <div class="form-group">
  {!! Form::submit('Add', ['class' => 'btn btn-primary']) !!}
  </div>
  {!!Form::close() !!}


@endsection

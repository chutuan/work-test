<div class="modal fade" id="modal-assign" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Assign member</h4>
      </div>
      {!! Form::open(['action' => ['TaskController@assign']]) !!}
      {!! Form::hidden('task_id', 0) !!}
      <div class="modal-body">
        <input type="text" name="email" class="form-control" autocomplete="off" id="autocomplete-assign"/>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      {!! Form::close() !!}
    </div>
  </div>
</div>

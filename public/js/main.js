$(document).ready(function()
{
  var taskID;
  var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
  $( ".column" ).sortable({
     connectWith: ".column",
     items: '.box-task',
     receive: function(event, ui)
     {
       $('.column').removeClass('border-blue').removeClass('border-green');


       var status = $(this).parent('.col-md-3').index();
       $.ajax({
            url: window.location.href  + '/tasks/'  + taskID ,
            type: 'POST',
            data: {_token: CSRF_TOKEN, _method: 'PATCH', status: status},
            dataType: 'JSON',
            success: function (data) {
            }
        });
     },
     start: function(event, ui)
     {
       $('.column').addClass('border-blue');
       $(this).removeClass('border-blue').removeClass('border-green');

       ajaxUpdateTask($(this));
     },
     stop: function(event, ui)
     {
       $('.column').removeClass('border-blue').removeClass('border-green');

     },
     over: function(event, ui)
     {
       $(this).addClass('border-green');

     },
     out: function(event, ui)
     {
       $(this).removeClass('border-green');
     }
   });

  //  BOX TASK CLICK function
  $('.box-task').on('click', function(e)
  {
    taskID = $(this).find('input[type="hidden"]').attr('value');
    if($(e.target).is('img'))
    {
      $('#modal-assign').find('input[name="task_id"]').attr('value', taskID);
      $('#modal-assign').modal('show');
      return;
    }
    ajaxUpdateTask($(this));
  });

  function ajaxUpdateTask(ui)
  {
    taskID = ui.find('input[type="hidden"]').attr('value');
    $('#box-task-detail .overlay').removeClass('hidden');
    $.ajax({
         url: window.location.href  + '/tasks/'  + taskID ,
         type: 'GET',

         success: function (data) {
           $('#box-task-detail .overlay').addClass('hidden');
            $('#box-task-detail .task-content').html(data);
         }
     });

  };

  $( "#autocomplete-user, #autocomplete-assign" ).autocomplete({
    serviceUrl: '/api/autocomplete',
    dataType: 'JSON'
  });

  

  // AJAX ASSIGN MEMBER
  $('#modal-assign form').submit(function(e)
  {
    e.preventDefault();
    $.ajax({
      url: $(this).attr('action'),
      type: 'POST',
      data: $(this).serialize(),
      success: function(data)
      {
        $('#modal-assign').modal('toggle');
        $this = $('.box-task').find('input[value="' + taskID + '"]').parents('.box-task')
        .find('img');
        $this.attr('src', '../imgs/avatar/' + data.avatar);
        $this.attr('title', data.name);
      }
    });
  });


});
